#include<iostream>
#include<vector>
#include<fstream>
#include<string>

using namespace std;

typedef struct contact
{
	int id;
	string fName;
	string lName;
	string email;
	string phone;
	string address;
} contact;

int addContact(vector<contact>& contacts, contact newContact);
void search(vector<contact>& contacts, string keyWord);
int remove(vector<contact>& contacts, int id);
int edit(vector<contact>& contacts, int id);
void import(vector<contact>& contacts);
void save(vector<contact> contacts);
int substr(string s1, string s2);
int isValid(vector<contact> contacts, contact newContact, int exc);
contact inputContact(void);

int main()
{
	string command="", keyWord;
	int id;
	contact newContact;
	vector<contact> contacts;
	import(contacts);
	while(cin>>command)
	{
		if(command=="add")
		{
			newContact=inputContact();
			if(contacts.size()==0)
				newContact.id=0;
			else
				newContact.id=(contacts[contacts.size()-1]).id+1;
			if(addContact(contacts, newContact))
				cout<<"Command Ok"<<endl;
			else
				cout<<"Command Failed"<<endl;
			cin.clear();
			fflush(stdin);
			save(contacts);
			continue;
		}
		else if(command=="search")
		{
			cin>>keyWord;
			search(contacts, keyWord);
			continue;
		}
		else if(command=="delete")
		{
			cin>>id;
			if(remove(contacts, id))
				cout<<"Command Ok"<<endl;
			else
				cout<<"Command Failed"<<endl;
			save(contacts);
			continue;
		}
		else if(command=="update")
		{
			cin>>id;
			if(edit(contacts, id))
				cout<<"Command Ok"<<endl;
			else
				cout<<"Command Failed"<<endl;
			cin.clear();
			fflush(stdin);
			save(contacts);
			continue;
		}
		else if(command=="show")
		{
			for(int i=0;i<contacts.size();i++)
				cout<<contacts[i].id<<" "<<contacts[i].fName<<" "<<contacts[i].lName<<" "<<contacts[i].phone<<" "<<contacts[i].email<<" "<<contacts[i].address<<endl;
		}
		else if(command=="exit")
			return 0;
		else
			cout<<"Command not found!"<<endl;
		cin.clear();
		fflush(stdin);
		command="";
	}
	return 0;
}

int addContact(vector<contact>& contacts, contact newContact)
{
	if(!isValid(contacts,newContact, -1))
		return 0;
	contacts.push_back(newContact);
	return 1;
}

void search(vector<contact>& contacts, string keyWord)
{
	for(int i=0;i<contacts.size();i++)
	{
		if(substr(contacts[i].fName,keyWord) || substr(contacts[i].lName,keyWord) || substr(contacts[i].email,keyWord) || substr(contacts[i].phone,keyWord) || substr(contacts[i].address,keyWord))
			cout<<contacts[i].id<<" "<<contacts[i].fName<<" "<<contacts[i].lName<<" "<<contacts[i].phone<<" "<<contacts[i].email<<" "<<contacts[i].address<<endl;
	}
}

int remove(vector<contact>& contacts, int id)
{
	for(int i=0;i<contacts.size();i++)
		if((contacts[i]).id==id)
		{
			contacts.erase(contacts.begin()+i);
			return 1;
		}
	return 0;
}

int edit(vector<contact>& contacts, int id)
{
	string s;
	char ch;
	int j;
	contact newContact;
	newContact.id=id;
	for(int i=0;i<contacts.size();i++)
		if(contacts[i].id==id)
		{
			j=i;
			newContact.fName=contacts[i].fName;
			newContact.lName=contacts[i].lName;
			newContact.phone=contacts[i].phone;
			newContact.email=contacts[i].email;
			newContact.address=contacts[i].address;
		}
	getline(cin, s);
	s+='\n';
	for(int i=0;i<s.length();i++)
	{
		if(s[i]=='-' && s[i+1]=='f')
		{
			newContact.fName="";
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				newContact.fName+=s[i];
		}
		else if(s[i]=='-' && s[i+1]=='l')
		{
			newContact.lName="";
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				newContact.lName+=s[i];
		}
		else if(s[i]=='-' && s[i+1]=='p')
		{
			newContact.phone="";
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				newContact.phone+=s[i];
		}
		else if(s[i]=='-' && s[i+1]=='e')
		{
			newContact.email="";
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				newContact.email+=s[i];
		}
		else if(s[i]=='-' && s[i+1]=='a')
		{
			newContact.address="";
			i+=2;
			for(i;s[i+1]!='\n'&&!(s[i+1]=='-'&&(s[i+2]=='f'||s[i+2]=='l'||s[i+2]=='e'||s[i+2]=='p'));i++)
				newContact.address+=s[i];
			newContact.address+=s[i];
		}
	}
	if(isValid(contacts, newContact, j))
	{
		contacts[j].fName=newContact.fName;
		contacts[j].lName=newContact.lName;
		contacts[j].email=newContact.email;
		contacts[j].phone=newContact.phone;
		return 1;
	}
	return 0;
}

void import(vector<contact>& contacts)
{
	char ch;
	ifstream i;
	ofstream o;
	o.open("contacts.csv", ios::app);
	o.close();
	i.open("contacts.csv", ios::in);
	while(!i.eof())
	{
		contact newContact;
		i>>newContact.id>>newContact.fName>>newContact.lName>>newContact.phone>>newContact.email;
		i.get(ch);
		getline(i, newContact.address);
		if(newContact.fName!="")
			contacts.push_back(newContact);
	}
	i.close();
}

void save(vector<contact> contacts)
{
	ofstream o;
	o.open("contacts.csv", ios::out);
	for(int i=0;i<contacts.size();i++)
	{
		o<<contacts[i].id<<" "<<contacts[i].fName<<" "<<contacts[i].lName<<" "<<contacts[i].phone<<" "<<contacts[i].email<<" "<<contacts[i].address<<"\n";
	}
	o.close();
}

int substr(string s1, string s2)
{
	for(int i=0;i<s1.length()-s2.length()+1;i++)
		if(s1.compare(i,s2.length(),s2))
			return 1;
	return 0;
}

int isValid(vector<contact> contacts, contact newContact, int exc)
{
	int eFlag=0;
	for(int i=0;i<contacts.size();i++)
		if(((contacts[i].fName==newContact.fName && contacts[i].lName==newContact.lName) ||contacts[i].email==newContact.email ||contacts[i].phone==newContact.phone) && i!=exc)
		return 0;

	if(newContact.phone[0]!='0' || newContact.phone[1]!='9' || newContact.phone.length()!=11)
		return 0;
	for(int i=2;i<11;i++)
		if(newContact.phone[i]<48 || newContact.phone[i]>57)
			return 0;

	for(int i=0;i<newContact.email.length()-1;i++)
	{
		if(newContact.email[i]=='@')
		{
			for(int j=i;j<newContact.email.length()-1;j++)
				if(newContact.email[j]=='.')
					eFlag=1;
				else if(newContact.email[j+1]=='@')
					return 0;
			if(newContact.email[i+1]=='.')
				return 0;
		}
	}
	if(!eFlag)
		return 0;
		
	for(int i=0;i<newContact.fName.length();i++)
		if(newContact.fName[i]==' ')
			return 0;
	for(int i=0;i<newContact.lName.length();i++)
		if(newContact.lName[i]==' ')
			return 0;
	for(int i=0;i<newContact.email.length();i++)
		if(newContact.email[i]==' ' || newContact.email[i]==',')
			return 0;
			
	if(newContact.fName=="-f" || newContact.fName=="-l" || newContact.fName=="-p" || newContact.fName=="-e")
		return 0;
	if(newContact.lName=="-f" || newContact.lName=="-l" || newContact.lName=="-p" || newContact.lName=="-e")
		return 0;
	if(newContact.phone=="-f" || newContact.phone=="-l" || newContact.phone=="-p" || newContact.phone=="-e")
		return 0;
	if(newContact.email=="-f" || newContact.email=="-l" || newContact.email=="-p" || newContact.email=="-e")
		return 0;
		
	return 1;
}

contact inputContact(void)
{
	contact New;
	New.fName="";
	New.lName="";
	New.phone="";
	New.email="";
	int fflag=0, lflag=0, pflag=0, eflag=0;
	string s;
	getline(cin, s);
	s+='\n';
	for(int i=0;i<s.length();i++)
	{
		if(s[i]=='-' && s[i+1]=='f')
		{
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				New.fName+=s[i];
			fflag=1;
		}
		else if(s[i]=='-' && s[i+1]=='l')
		{
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				New.lName+=s[i];
			lflag=1;
		}
		else if(s[i]=='-' && s[i+1]=='p')
		{
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				New.phone+=s[i];
			pflag=1;
		}
		else if(s[i]=='-' && s[i+1]=='e')
		{
			i+=2;
			while(s[i]==' ' || s[i]=='	' || s[i]=='\n')
				i++;
			for(i;s[i]!=' ' && s[i]!='	' && s[i]!='\n';i++)
				New.email+=s[i];
			eflag=1;
		}
		else if(s[i]=='-' && s[i+1]=='a')
		{
			i+=2;
			for(i;s[i+1]!='\n'&&!(s[i+1]=='-'&&(s[i+2]=='f'||s[i+2]=='l'||s[i+2]=='e'||s[i+2]=='p'));i++)
				New.address+=s[i];
			New.address+=s[i];
		}
	}
	if(fflag==0 || lflag==0 || pflag==0 || eflag==0)
		New.phone="WRONG";
	return New;
}

